<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->primary('id');
            $table->string('name')->nullable();
            
            $table->unsignedInteger('rank')->nullable();
            
            $table->unsignedInteger('kills')->default(0);
            $table->unsignedInteger('deaths')->default(0);
            $table->unsignedInteger('teamkills')->default(0);
            $table->decimal('ratio', 5, 2)->default(0);

            $table->unsignedInteger('slays')->default(0);
            $table->unsignedInteger('kicks')->default(0);
            $table->unsignedInteger('temp_bans')->default(0);
            $table->unsignedInteger('perm_bans')->default(0);
            
            $table->boolean('muted')->default(false);
            $table->boolean('admin')->default(false);
            
            $table->string('ip')->nullable();
            $table->string('face')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}

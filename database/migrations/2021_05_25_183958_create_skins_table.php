<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skins', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            
            $table->integer('head_id')->default(-1);
            $table->integer('body_id')->default(-1);
            $table->integer('foot_id')->default(-1);
            $table->integer('hand_id')->default(-1);
            
            $table->boolean('usable_by_all')->default(false);
            $table->boolean('usable_by_admins')->default(false);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skins');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        
        $username = 'admin';
        $password = Str::random(32);
        
        $user->username = $username;
        $user->password = bcrypt($password);
        $user->manage_users = true;
        
        $user->save();
        
        echo "Admin user added: $username - $password\n";
    }
}

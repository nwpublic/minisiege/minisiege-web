<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Map;

class MapSeeder extends Seeder
{
    protected $maps = [
        16 => ["mp_ambush", "Ambush (Day)"],
        17 => ["mp_ambush_fog", "Ambush (Fog)"],
        18 => ["mp_arabian_harbour", "Arabian Harbour (Day)"],
        19 => ["mp_arabian_harbour_night", "Arabian Harbour (Night)"],
        20 => ["mp_arabian_village", "Arabian Village (Day)"],
        21 => ["mp_arabian_village_morning", "Arabian Village (Morning)"],
        22 => ["mp_arabian_village_conq", "Arabian Town (Day)"],
        23 => ["mp_arabian_village_conq_morning", "Arabian Town (Morning)"],
        24 => ["mp_ardennes", "Ardennes (Snowing)"],
        25 => ["mp_ardennes_morning", "Ardennes (Morning)"],
        26 => ["mp_avignon", "Avignon (Day)"],
        27 => ["mp_avignon_morning", "Avignon (Cloudy)"],
        28 => ["mp_bavarian_river", "Bavarian River (Day)"],
        29 => ["mp_bavarian_river_cloudy", "Bavarian River (Cloudy)"],
        30 => ["mp_beach", "Beach (Day)"],
        31 => ["mp_beach_morning", "Beach (Morning)"],
        32 => ["mp_borodino", "Borodino (Day)"],
        33 => ["mp_borodino_morn", "Borodino (Morning)"],
        34 => ["mp_champs_elysees", "Champs-Elysees (Day)"],
        35 => ["mp_champs_elysees_rain", "Champs-Elysees (Raining)"],
        36 => ["mp_charge_to_the_rhine", "Charge to the Rhine (Day)"],
        37 => ["mp_charge_to_the_rhine_cloudy", "Charge to the Rhine (Cloudy)"],
        38 => ["mp_citadelle_napoleon", "Citadelle Napoleon (Day)"],
        39 => ["mp_citadelle_napoleon_morning", "Citadelle Napoleon (Morning)"],
        40 => ["mp_columbia_hill_farm", "Columbia Farm (Day)"],
        41 => ["mp_columbia_farm_morning", "Columbia Farm (Morning)"],
        42 => ["mp_countryside", "Countryside (Day)"],
        43 => ["mp_countryside_fog", "Countryside (Fog)"],
        44 => ["mp_dust", "Dust (Day)"],
        45 => ["mp_dust_morning", "Dust (Morning)"],
        46 => ["mp_european_city_summer", "European City (Summer)"],
        47 => ["mp_european_city_winter", "European City (Winter)"],
        48 => ["mp_floodplain", "Floodplain (Day)"],
        49 => ["mp_floodplain_storm", "Floodplain (Storm)"],
        50 => ["mp_forest_pallisade", "Forest Pallisade (Day)"],
        51 => ["mp_forest_pallisade_fog", "Forest Pallisade (Fog)"],
        52 => ["mp_fort_al_hafya", "Fort Al Hafya (Day)"],
        53 => ["mp_fort_al_hafya_night", "Fort Al Hafya (Night)"],
        54 => ["mp_fort_bashir", "Fort Bashir (Day)"],
        55 => ["mp_fort_bashir_morning", "Fort Bashir (Morning)"],
        56 => ["mp_fort_beaver", "Fort Fausbourg (Day)"],
        57 => ["mp_fort_beaver_morning", "Fort Fausbourg (Morning)"],
        58 => ["mp_fort_boyd", "Fort Boyd (Day)"],
        59 => ["mp_fort_boyd_raining", "Fort Boyd (Raining)"],
        60 => ["mp_fort_brochet", "Fort Brochet (Day)"],
        61 => ["mp_fort_brochet_raining", "Fort Brochet (Raining)"],
        62 => ["mp_fort_de_chartres", "Fort de Chartres (Day)"],
        63 => ["mp_fort_de_chartres_raining", "Fort de Chartres (Raining)"],
        64 => ["mp_fort_fleetwood", "Fort Fleetwood (Morning)"],
        65 => ["mp_fort_fleetwood_storm", "Fort Fleetwood (Storm)"],
        66 => ["mp_fort_george", "Fort George (Day)"],
        67 => ["mp_fort_george_raining", "Fort George (Raining)"],
        68 => ["mp_fort_hohenfels", "Fort Hohenfels (Day)"],
        69 => ["mp_fort_hohenfels_night", "Fort Hohenfels (Night)"],
        70 => ["mp_fort_lyon", "Fort Lyon (Day)"],
        71 => ["mp_fort_lyon_night", "Fort Lyon (Night)"],
        72 => ["mp_fort_mackinaw", "Fort Mackinaw (Day)"],
        73 => ["mp_fort_mackinaw_raining", "Fort Mackinaw (Raining)"],
        74 => ["mp_fort_nylas", "Fort Nylas (Day)"],
        75 => ["mp_fort_nylas_raining", "Fort Nylas (Raining)"],
        76 => ["mp_fort_refleax", "Fort Willsbridge (Day)"],
        77 => ["mp_fort_refleax_night", "Fort Willsbridge (Night)"],
        78 => ["mp_fort_vincey", "Fort Whittington (Day)"],
        79 => ["mp_fort_vincey_storm", "Fort Whittington (Storm)"],
        80 => ["mp_french_farm", "French Farm (Day)"],
        81 => ["mp_french_farm_storm", "French Farm (Storm)"],
        82 => ["mp_german_village", "German Village (Morning)"],
        83 => ["mp_german_village_rain", "German Village (Storm)"],
        84 => ["mp_hougoumont", "Hougoumont (Day)"],
        85 => ["mp_hougoumont_night", "Hougoumont (Night)"],
        86 => ["mp_hungarian_plains", "Hungarian Plain (Day)"],
        87 => ["mp_hungarian_plains_cloud", "Hungarian Plain (Cloudy)"],
        88 => ["mp_theisland", "The Island (Day)"],
        89 => ["mp_la_haye_sainte", "La Haye Sainte (Day)"],
        90 => ["mp_la_haye_sainte_night", "La Haye Sainte (Night)"],
        91 => ["mp_landshut", "Landshut (Day)"],
        92 => ["mp_landshut_night", "Landshut (Night)"],
        93 => ["mp_minden", "Minden (Day)"],
        94 => ["mp_minden_night", "Minden (Morning)"],
        95 => ["mp_naval", "Naval Battle (Day)"],
        96 => ["mp_oaksfield_day", "Oaksfield (Day)"],
        97 => ["mp_oaksfield_storm", "Oaksfield (Storm)"],
        98 => ["mp_outlaws_den", "Outlaw's Den (Day)"],
        99 => ["mp_outlaws_den_night", "Outlaw's Den (Night)"],
        100 => ["mp_pyramids", "Battle of the Pyramids (Day)"],
        101 => ["mp_quatre_bras", "Quatre Bras (Day)"],
        102 => ["mp_quatre_bras_night", "Quatre Bras (Night)"],
        103 => ["mp_river_crossing", "River Crossing (Day)"],
        104 => ["mp_river_crossing_morning", "River Crossing (Morning)"],
        105 => ["mp_roxburgh", "Roxburgh (Day)"],
        106 => ["mp_roxburgh_raining", "Roxburgh (Raining)"],
        107 => ["mp_russian_river_day", "Russian River (Day)"],
        108 => ["mp_russian_river_cloudy", "Russian River (Raining)"],
        109 => ["mp_russian_village", "Russian Village (Snow)"],
        110 => ["mp_russian_village_fog", "Russian Village (Fog)"],
        111 => ["mp_russian_village_conq", "Russian Town (Fog)"],
        112 => ["mp_russian_village_conq_night", "Russian Town (Night)"],
        113 => ["mp_saints_isle", "Saint's Isle (Day)"],
        114 => ["mp_saints_isle_rain", "Saint's Isle (Rain)"],
        115 => ["mp_schemmerbach", "Schemmerbach (Day)"],
        116 => ["mp_schemmerbach_storm", "Schemmerbach (Storm)"],
        117 => ["mp_siege_of_toulon", "Siege of Toulon (Day)"],
        118 => ["mp_siege_of_toulon_night", "Siege of Toulon (Night)"],
        119 => ["mp_sjotofta", "Sjotofta (Day)"],
        120 => ["mp_sjotofta_night", "Sjotofta (Night)"],
        121 => ["mp_slovenian_village", "Slovenian Village (Day)"],
        122 => ["mp_slovenian_village_raining", "Slovenian Village (Raining)"],
        123 => ["mp_spanish_farm", "Spanish Farm (Day)"],
        124 => ["mp_spanish_farm_rain", "Spanish Farm (Raining)"],
        125 => ["mp_spanish_mountain_pass", "Spanish Mountain Pass (Day)"],
        126 => ["mp_spanish_mountain_pass_evening", "Spanish Mountain Pass (Evening)"],
        127 => ["mp_spanish_village", "Spanish Village (Day)"],
        128 => ["mp_spanish_village_evening", "Spanish Village (Evening)"],
        129 => ["mp_strangefields", "Strangefields"],
        130 => ["mp_strangefields_storm", "Strangefields (Storm)"],
        131 => ["mp_swamp", "Siegburger Swamp (Fog)"],
        132 => ["mp_venice", "Venice (Day)"],
        133 => ["mp_venice_morning", "Venice (Morning)"],
        134 => ["mp_walloon_farm", "Wallonian Farm (Day)"],
        135 => ["mp_walloon_farm_night", "Wallonian Farm (Night)"],
        136 => ["mp_wissaudorf", "Wissaudorf (Day)"],
        137 => ["mp_testing_map", "Testing map"],
        162 => ["mp_custom_map_1", "Custom Map 1"],
        163 => ["mp_custom_map_2", "Custom Map 2"],
        164 => ["mp_custom_map_3", "Custom Map 3"],
        165 => ["mp_custom_map_4", "Custom Map 4"],
        166 => ["mp_custom_map_5", "Custom Map 5"],
        167 => ["mp_custom_map_6", "Custom Map 6"],
        168 => ["mp_custom_map_7", "Custom Map 7"],
        169 => ["mp_custom_map_8", "Custom Map 8"],
        170 => ["mp_custom_map_9", "Custom Map 9"],
        171 => ["mp_custom_map_10", "Custom Map 10"],
        172 => ["mp_custom_map_11", "Custom Map 11"],
        173 => ["mp_custom_map_12", "Custom Map 12"],
        174 => ["mp_custom_map_13", "Custom Map 13"],
        175 => ["mp_custom_map_14", "Custom Map 14"],
        176 => ["mp_custom_map_15", "Custom Map 15"],
        177 => ["mp_custom_map_16", "Custom Map 16"],
        178 => ["mp_custom_map_17", "Custom Map 17"],
        179 => ["mp_custom_map_18", "Custom Map 18"],
        180 => ["mp_custom_map_19", "Custom Map 19"],
        181 => ["mp_custom_map_20", "Custom Map 20"],
        182 => ["mp_custom_map_21", "Custom Map 21"],
        183 => ["mp_custom_map_22", "Custom Map 22"],
        184 => ["mp_custom_map_23", "Custom Map 23"],
        185 => ["mp_custom_map_24", "Custom Map 24"],
        186 => ["mp_custom_map_25", "Custom Map 25"],
        187 => ["mp_custom_map_26", "Custom Map 26"],
        188 => ["mp_custom_map_27", "Custom Map 27"],
        189 => ["mp_custom_map_28", "Custom Map 28"],
        190 => ["mp_custom_map_29", "Custom Map 29"],
        191 => ["mp_custom_map_30", "Custom Map 30"],
        192 => ["mp_custom_map_31", "Custom Map 31"],
        193 => ["mp_custom_map_32", "Custom Map 32"],
        194 => ["mp_custom_map_33", "Custom Map 33"],
        195 => ["mp_custom_map_34", "Custom Map 34"],
        196 => ["mp_custom_map_35", "Custom Map 35"],
        197 => ["mp_custom_map_36", "Custom Map 36"],
        198 => ["mp_custom_map_37", "Custom Map 37"],
        199 => ["mp_custom_map_38", "Custom Map 38"],
        200 => ["mp_custom_map_39", "Custom Map 39"],
        201 => ["mp_custom_map_40", "Custom Map 40"],
        202 => ["mp_custom_map_41", "Custom Map 41"],
        203 => ["mp_custom_map_42", "Custom Map 42"],
        204 => ["mp_custom_map_43", "Custom Map 43"],
        205 => ["mp_custom_map_44", "Custom Map 44"],
        206 => ["mp_custom_map_45", "Custom Map 45"],
        207 => ["mp_custom_map_46", "Custom Map 46"],
        208 => ["mp_custom_map_47", "Custom Map 47"],
        209 => ["mp_custom_map_48", "Custom Map 48"],
        210 => ["mp_custom_map_49", "Custom Map 49"],
        211 => ["mp_custom_map_50", "Custom Map 50"],
        212 => ["mp_custom_map_51", "Custom Map 51"],
        213 => ["mp_custom_map_52", "Custom Map 52"],
        214 => ["mp_custom_map_53", "Custom Map 53"],
        215 => ["mp_custom_map_54", "Custom Map 54"],
        216 => ["mp_custom_map_55", "Custom Map 55"],
        217 => ["mp_custom_map_56", "Custom Map 56"],
        218 => ["mp_custom_map_57", "Custom Map 57"],
        219 => ["mp_custom_map_58", "Custom Map 58"],
        220 => ["mp_custom_map_59", "Custom Map 59"],
        221 => ["mp_custom_map_60", "Custom Map 60"],
        222 => ["mp_custom_map_61", "Custom Map 61"],
        223 => ["mp_custom_map_62", "Custom Map 62"],
        224 => ["mp_custom_map_63", "Custom Map 63"],
        225 => ["mp_custom_map_64", "Custom Map 64"],
        226 => ["mp_custom_map_65", "Custom Map 65"],
        227 => ["mp_custom_map_66", "Custom Map 66"],
        228 => ["mp_custom_map_67", "Custom Map 67"],
        229 => ["mp_custom_map_68", "Custom Map 68"],
        230 => ["mp_custom_map_69", "Custom Map 69"],
        231 => ["mp_custom_map_70", "Custom Map 70"],
        232 => ["mp_custom_map_71", "Custom Map 71"],
        233 => ["mp_custom_map_72", "Custom Map 72"],
        234 => ["mp_custom_map_73", "Custom Map 73"],
        235 => ["mp_custom_map_74", "Custom Map 74"],
        236 => ["mp_custom_map_75", "Custom Map 75"],
        237 => ["mp_custom_map_76", "Custom Map 76"],
        238 => ["mp_custom_map_77", "Custom Map 77"],
        239 => ["mp_custom_map_78", "Custom Map 78"],
        240 => ["mp_custom_map_79", "Custom Map 79"],
        241 => ["mp_custom_map_80", "Custom Map 80"],
        242 => ["mp_custom_map_81", "Custom Map 81"],
        243 => ["mp_custom_map_82", "Custom Map 82"],
        244 => ["mp_custom_map_83", "Custom Map 83"],
        245 => ["mp_custom_map_84", "Custom Map 84"],
        246 => ["mp_custom_map_85", "Custom Map 85"],
        247 => ["mp_custom_map_86", "Custom Map 86"],
        248 => ["mp_custom_map_87", "Custom Map 87"],
        249 => ["mp_custom_map_88", "Custom Map 88"],
        250 => ["mp_custom_map_89", "Custom Map 89"],
        251 => ["mp_custom_map_90", "Custom Map 90"],
        252 => ["mp_custom_map_91", "Custom Map 91"],
        253 => ["mp_custom_map_92", "Custom Map 92"],
        254 => ["mp_custom_map_93", "Custom Map 93"],
        255 => ["mp_custom_map_94", "Custom Map 94"],
        256 => ["mp_custom_map_95", "Custom Map 95"],
        257 => ["mp_custom_map_96", "Custom Map 96"],
        258 => ["mp_custom_map_97", "Custom Map 97"],
        259 => ["mp_custom_map_98", "Custom Map 98"],
        260 => ["mp_custom_map_99", "Custom Map 99"],
        261 => ["mp_custom_map_100", "Custom Map 100"],
	];
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->maps as $mapId => $mapStrings) {
            $map = new Map;
            
            $map->id = $mapId;
            $map->string_id = $mapStrings[0];
            $map->name = $mapStrings[1];
            
            $map->save();
        }
    }
}

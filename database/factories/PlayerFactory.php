<?php

namespace Database\Factories;

use App\Models\Player;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlayerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Player::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->unique()->randomNumber(5, false),
            'name' => $this->faker->name(),
            
            'kills' => $this->faker->randomNumber(3, false),
            'deaths' => $this->faker->randomNumber(3, false),
            'teamkills' => $this->faker->randomNumber(3, false),
        ];
    }
}

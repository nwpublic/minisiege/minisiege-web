<?php

use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\SkinController;
use App\Http\Controllers\MBApiController;


Route::get('/', [DashboardController::class, 'show'])->name('dashboard');

Route::get('/leaderboard', [PlayerController::class, 'leaderboard'])->name('leaderboard');
Route::get('/maps', [MapController::class, 'votable'])->name('maps');

Route::get('/login', [AuthenticateController::class, 'showLogin'])->name('auth.login');
Route::post('/login', [AuthenticateController::class, 'login']);

Route::middleware('auth')->group(function() {
    Route::middleware('can:admin,App\Models\User')->group(function() {
        Route::name('users.')->group(function() {
            Route::get('/users', [UserController::class, 'index'])->name('index');
            Route::post('/users', [UserController::class, 'create'])->name('create');
            Route::get('/user/{user}', [UserController::class, 'get'])->name('get');
            Route::patch('/user/{user}', [UserController::class, 'update'])->name('update');
            Route::delete('/user/{user}', [UserController::class, 'delete'])->name('delete');
        });
    });
    
    Route::delete('/leaderboard', [PlayerController::class, 'clearLeaderboard'])->name('leaderboard.clear');
    Route::name('players.')->group(function() {
        Route::get('/player/{player}', [PlayerController::class, 'get'])->name('get');
    });
    
    Route::name('server.')->group(function() {
        Route::post('/server/recompile', [DashboardController::class, 'recompile'])->name('recompile');
        
        Route::get('/server/settings', [SettingController::class, 'index'])->name('settings.index');
        Route::patch('/server/settings', [SettingController::class, 'update'])->name('settings.update');
        
        Route::name('maps.')->group(function() {
            Route::get('/server/maps', [MapController::class, 'index'])->name('index');
            Route::get('/server/map/{map}', [MapController::class, 'get'])->name('get');
            Route::patch('/server/map/{map}', [MapController::class, 'update'])->name('update');
        });
        
        Route::name('skins.')->group(function() {
            Route::get('/server/skins', [SkinController::class, 'index'])->name('index');
            
            Route::post('/server/skins', [SkinController::class, 'skinCreate'])->name('skinCreate');
            Route::get('/server/skin/{skin}', [SkinController::class, 'skinGet'])->name('skinGet');
            Route::patch('/server/skin/{skin}', [SkinController::class, 'skinUpdate'])->name('skinUpdate');
            Route::delete('/server/skin/{skin}', [SkinController::class, 'skinDelete'])->name('skinDelete');
            
            Route::post('/server/skin/group', [SkinController::class, 'groupCreate'])->name('groupCreate');
            Route::get('/server/skin/group/{group}', [SkinController::class, 'groupGet'])->name('groupGet');
            Route::patch('/server/skin/group/{group}', [SkinController::class, 'groupUpdate'])->name('groupUpdate');
            Route::delete('/server/skin/group/{group}', [SkinController::class, 'groupDelete'])->name('groupDelete');
        });
        
        Route::name('admins.')->group(function() {
            Route::get('/server/admins', [PlayerController::class, 'adminsGet'])->name('get');
            Route::patch('/server/admins', [PlayerController::class, 'adminsUpdate'])->name('update');
        });
        
        Route::name('mutes.')->group(function() {
            Route::get('/server/mutes', [PlayerController::class, 'mutesGet'])->name('get');
            Route::patch('/server/mutes', [PlayerController::class, 'mutesUpdate'])->name('update');
        });
    });
    
    Route::post('/logout', [AuthenticateController::class, 'logout'])->name('auth.logout');
});

Route::get('/mb/{key}/{action}', [MBApiController::class, 'handle']);
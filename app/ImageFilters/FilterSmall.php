<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;
use App\ImageFilters\FilterResize;

class FilterSmall extends FilterResize
{
    protected $width = 300;
}
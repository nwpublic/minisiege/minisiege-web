<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class FilterResize implements FilterInterface
{
    protected $height = null;
    protected $width = null;
    
    public function applyFilter(Image $image)
    {
        return $image->resize($this->width, $this->height, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');;
    }
}
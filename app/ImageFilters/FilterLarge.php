<?php

namespace App\ImageFilters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;
use App\ImageFilters\FilterResize;

class FilterLarge extends FilterResize
{
    protected $width = 1000;
}
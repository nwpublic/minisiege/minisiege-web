<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Setting;
use App\Models\Player;
use App\Models\Map;
use App\Models\Skin;
use App\Jobs\CompileModule;
use App\BanList;

class MBApiController extends Controller
{
    protected $response = [
        "map_settings" => 2,
        "welcome_message" => 3,
        "parse_command" => 5,
        "player_misdeeds" => 6,
    ];
    
    protected $command = [
        "error" => -1,
        "skin" => 1,
        "skins" => 2,
        "find" => 7,
    ];
    
    protected function fixEncoding($string) {
        return mb_convert_encoding($string, "Windows-1252", "UTF-8");
    }
    
    protected function filterString($string) {
        return str_replace(['UNRECOGNIZED TOKEN ', '|', '^', '{', '}', '<', '>', "\r", "\n"], '', $string);
    }
    
    protected function doValidation($input, $rules) {
        $validator = Validator::make($input, $rules);
        
        if ($validator->fails()) {
            throw new \Exception(implode(' ', $validator->errors()->all()));
        }
        
        return $validator;
    }
    
    public function handle(Request $request, $key, $action) {
        $response = [0];
        
        try {
            if ($key !== Setting::get('serverApiKey')) {
                throw new \Exception('Auth Error');
            }
            
            $server_ip = Setting::get('serverAddress') ?? '127.0.0.1';
            if (!Setting::get('serverApiInscure')) {
                
                $request_ip = $request->ip();
                if (Setting::get('serverApiCloudflare') && $request->hasHeader('CF-Connecting-IP')) {
                    $request_ip = $request->header('CF-Connecting-IP');
                }
                
                if ($request_ip !== $server_ip) {
                    throw new \Exception('Auth Error');
                }
            }
            
            if ($action === 'welcome') {
                $this->doValidation($request->all(), [
                    'id' => 'required|integer|min:1',
                    'playerid' => 'required|integer',
                    'name' => 'required|string',
                    'face' => 'required|string',
                    'ip' => 'required|ipv4',
                ]);
                
                $guid = $request->input('id');
                $name = $this->fixEncoding($request->input('name'));
                $ip = $request->input('ip');
                $face = $request->input('face');
                
                $player = Player::updateOrCreate(
                    [
                        'id' => $guid,
                    ], [
                        'name' => $name,
                        'ip' => $ip,
                        'face' => $face,
                    ]
                );
                
                $ban = BanList::checkBan($guid, $name, $face, $ip) ? 1 : 0;
                
                $response[] = $this->response['welcome_message'];
                $response[] = $request->input('playerid');
                $response[] = 0; // ok
                $response[] = ' ' . $this->filterString($name);
                $response[] = ' ' . $this->filterString($player->updated_at);
                $response[] = $player->kills;
                $response[] = $player->deaths;
                $response[] = $player->teamkills;
                $response[] = ' ' . $player->ratio;
                $response[] = $player->rank ?? '-1';
                $response[] = $guid;
                $response[] = $player->admin;
                $response[] = $ban;
                $response[] = $player->muted;
                $response[] = $player->slays;
                $response[] = $player->kicks;
                $response[] = $player->temp_bans;
                $response[] = $player->perm_bans;
            } elseif ($action === 'log') {
                $this->doValidation($request->all(), [
                    'id' => 'required|integer|exists:players,id',
                    'field' => 'required|string',
                    'value' => 'required',
                    'additional' => 'nullable|integer|exists:players,id'
                ]);
                
                $player = Player::find($request->input('id'));
                $field = $request->input('field');
                $value = $request->input('value');
                
                $return_misdeeds = false;
                switch ($field) {
                    case 'kills':
                        $player->increment('kills', $value);
                        break;
                    case 'deaths':
                        $player->increment('deaths', $value);
                        break;
                    case 'teamkills':
                        $player->increment('teamkills', $value);
                        break;
                    case 'slays':
                        $player->increment('slays', $value);
                        $return_misdeeds = true;
                        break;
                    case 'kicks':
                        $player->increment('kicks', $value);
                        $return_misdeeds = true;
                        break;
                    case 'tempbans':
                        $player->increment('temp_bans', $value);
                        $return_misdeeds = true;
                        break;
                    case 'permbans':
                        $player->increment('perm_bans', $value);
                        $return_misdeeds = true;
                        break;
                    default:
                        throw new \Exception('Bad field');
                }
                
                if ($request->has('additional')) {
                    if ($field === 'kills' || $field === 'teamkills') {
                        $additonal_player = Player::find($request->input('additional'));
                        $additonal_player->increment('deaths', 1);
                    }
                }
                
                if ($return_misdeeds) {
                    $response[] = $this->response['player_misdeeds'];
                    $response[] = $player->id;
                    $response[] = $player->slays;
                    $response[] = $player->kicks;
                    $response[] = $player->temp_bans;
                    $response[] = $player->perm_bans;
                    $response[] = $player->muted;
                }
            } elseif ($action === 'map') {
                $this->doValidation($request->all(), [
                    'id' => 'required|integer|exists:maps,id',
                ]);
                
                $map = Map::find($request->input('id'));
                
                $response[] = $this->response['map_settings'];
                $response[] = $map->id;
                $response[] = $map->settings['set_round_max_seconds'] ?? -1;
            } elseif ($action === 'mute') {
                $this->doValidation($request->all(), [
                    'id' => 'required|integer|exists:players,id',
                ]);
                
                $player = Player::find($request->input('id'));
                $player->muted = 1;
                $player->save();
                
                CompileModule::dispatch();
            } elseif ($action === 'cmd2') {
                $this->doValidation($request->all(), [
                    'guid' => 'required|integer|exists:players,id',
                    'playerid' => 'required|integer',
                    'str' => 'required|string',
                ]);
                
                $str = $this->fixEncoding($request->input('str'));
                $guid = $request->input('guid');
                
                $response[] = $this->response['parse_command'];
                $response[] = $request->input('playerid');
                
                if (strpos($str, '/') === 0) {
                    $response[] = 0;
                    
                    $cmd = explode(' ', $str, 2);
                    switch ($cmd[0]) {
                        case "/skins":
                            $response[] = $this->command["skins"];
                            
                            $player = Player::find($guid);
                            
                            $skins = $player->skins();
                            
                            $response[] = $skins->count();
                            foreach ($skins as $skin) {
                                $response[] = '* ' . $this->filterString($skin->name);
                            }
                        break;
                        case "/skin":
                            $response[] = $this->command["skin"];
                            
                            if (count($cmd) > 1 && $cmd[1] !== '') {
                                $skin = Skin::where('name', 'like', "%{$cmd[1]}%")->first();
                                
                                if ($skin === null) {
                                    $response[] = -1;
                                } else {
                                    $allowed_skins = Player::find($guid)->skins();
                                    
                                    if ($allowed_skins->contains($skin)) {
                                        $response[] = 1;
                                        $response[] = $this->filterString($skin->name) . ' ';
                                        $response[] = $skin->head_id;
                                        $response[] = $skin->body_id;
                                        $response[] = $skin->foot_id;
                                        $response[] = $skin->hand_id;
                                        
                                    } else {
                                        $response[] = 0;
                                    }
                                }
                            } else {
                                $response[] = -1;
                            }
                        break;
                        case "/find":
                            $response[] = $this->command["find"];
                            
                            if (count($cmd) > 1 && $cmd[1] !== '') {
                                $search = $cmd[1];
                                
                                $player = Player::where('name', $search)
                                                ->orWhere('name', 'like', "%$search%")
                                                ->orderBy('updated_at', 'desc')
                                                ->first();
                                
                                if ($player !== null) {
                                    $response[] = "$player->name ($player->id) Slays: $player->slays Kicks: $player->kicks Temps: $player->temp_bans Perms: $player->perm_bans Muted: "
                                                . ($player->muted ? 'yes' : 'no');
                                } else {
                                    $response[] = 'Player not found.';
                                }
                            } else {
                                $response[] = 'Specify a player.';
                            }
                        break;
                        default:
                            $response[] = $this->command["error"];
                    }
                } else {
                    $response[] = 1;
                    $response[] = $this->filterString($str) . ' ';
                }
            } else {
                throw new \Exception('Invalid action');
            }
        } catch (\Exception $e) {
            $response = [
                -1,
                get_class($e) . ' => ' . $e->getMessage(),
            ];
        }
        
        return response(implode('|', $response))->header('Content-Type', 'text/plain');
    }
}

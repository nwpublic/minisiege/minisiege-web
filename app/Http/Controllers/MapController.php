<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use App\Models\Map;
use App\Jobs\CompileModule;

class MapController extends Controller
{
    protected $mission_types = [
        'dm', 'tdm', 'hq', 'cf',
        'sg', 'bt', 'cb', 'duel',
        'kh', 'br', 'cm',
    ];
    
    public function votable() {
        $maps = Map::select(['id', 'name', 'custom_name', 'min_players', 'max_players', 'admin_votable', 'player_votable', 'image'])
                   ->where('admin_votable', 1)->orWhere('player_votable', 1)->get();
        
        return Inertia::render('MapsVotable', [
            'title' => 'Votable Maps',
            'maps' => $maps,
        ]);
    }
    
    public function index() {
        $maps = Map::all();
        
        return Inertia::render('MapsIndex', [
            'title' => 'Manage Maps',
            'maps' => $maps,
        ]);
    }
    
    public function get(Map $map) {
        return Inertia::render('MapsEdit', [
            'title' => 'Edit Map',
            'map' => $map,
            'mission_types' => $this->mission_types,
        ]);
    }
    
    public function update(Request $request, Map $map) {
        $validated = $request->validate([
            'custom_name' => 'nullable|string|max:100',
            'min_players' => 'nullable|integer|min:0|max:500',
            'max_players' => 'nullable|integer|min:0|max:500',
            'admin_votable' => 'boolean',
            'player_votable' => 'boolean',
            'settings.mission_type' => [
                'nullable',
                Rule::in($this->mission_types),
            ],
            'settings.set_round_max_seconds' => 'nullable|integer|min:0',
            'settings.set_map_defender_bp' => 'nullable|integer|min:0',
            'settings.set_map_attacker_bp' => 'nullable|integer|min:0',
            'settings.set_respawn_period' => 'nullable|integer|min:0',
            'settings.set_team_point_limit' => 'nullable|integer|min:0',
            'settings.set_team_points_gained_for_flags' => 'nullable|integer|min:0',
        ]);
        
        $validated_files = $request->validate([
            'uploaded_image' => 'nullable|file',
        ]);
        
        if (isset($validated_files['uploaded_image'])) {
            $validated_files['uploaded_image']->store('images', 'public');
            $map->image = $validated_files['uploaded_image']->hashName();
        }
        
        $map->fill($validated);
        $map->save();
        
        CompileModule::dispatch();
        
        return back();
    }
}
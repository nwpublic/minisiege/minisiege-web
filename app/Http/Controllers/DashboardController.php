<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Setting;
use App\Jobs\CompileModule;

class DashboardController extends Controller
{
    public function show() {
        return Inertia::render('Dashboard', [
            'serverStatus' => Setting::get('status'),
            'compiling' => Auth::user() ? Setting::get('compiling') : null,
            'title' => 'Dashboard',
        ]);
    }
    
    public function recompile() {
        Setting::set('compiling', true);
        CompileModule::dispatch();
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;
use Illuminate\Support\Str;
use App\Models\User;

class UserController extends Controller
{
    public function index() {
        return Inertia::render('UsersIndex', [
            'users' => User::all(),
            'randomPassword' => Str::random(32),
            'title' => 'Panel Users',
        ]);
    }
    
    public function create(Request $request) {
        $validated = $request->validate([
            'username' => 'required|string|alpha_dash|unique:users',
            'password' => 'required|string',
        ]);
        
        $user = new User;
        $user->username = $validated['username'];
        $user->password = Hash::make($validated['password']);
        $user->save();
        
        return redirect()->route('users.get', $user);
    }
    
    public function get(User $user) {
        return Inertia::render('UsersEdit', [
            'isSelf' => Auth::user() == $user,
            'manangeUser' => $user,
            'title' => 'Panel User ' . $user->username,
        ]);
    }
    
    public function update(Request $request, User $user) {
        $validated = $request->validate([
            'username' => [
                'required',
                'string',
                'alpha_dash',
                Rule::unique('users')->ignore($user),
            ],
            'password' => 'nullable|string',
            'manage_users' => 'boolean',
        ]);
        
        $user->username = $validated['username'];
        $user->manage_users = $validated['manage_users'];
        if ($validated['password'] !== null) {
            $user->password = Hash::make($validated['password']);
        }
        $user->save();
        
        return back();
    }
    
    public function delete(User $user) {
        if (Auth::user() != $user) {
            $user->delete();
        }
        
        return redirect()->route('users.index');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Setting;
use App\Jobs\CompileModule;

class SettingController extends Controller
{
    protected $editableSettings = [
        'serverName' => 'nullable|string',
        'serverAddress' => 'nullable|ipv4',
        'serverPort' => 'nullable|integer',
        'serverApiKey' => 'required|string',
        'serverApiInscure' => 'boolean',
        'serverApiCloudflare' => 'boolean',
        'serverWelcome' => 'nullable|string',
        'serverAnnouncements' => 'array',
        'serverAnnouncements.*' => 'string',
    ];
    
    public function index() {
        $settings = Setting::select('setting', 'value')->whereIn('setting', array_keys($this->editableSettings))->get();
        
        return Inertia::render('Settings', [
            'title' => 'Settings',
            'settings' => $settings,
        ]);
    }
    
    public function update(Request $request) {
        $validated = $request->validate($this->editableSettings);
        
        Setting::applySettings($validated);
        
        CompileModule::dispatch();
        
        return back();
    }
}

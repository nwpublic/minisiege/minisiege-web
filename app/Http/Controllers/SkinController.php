<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use App\Models\Skin;
use App\Models\SkinGroup;

class SkinController extends Controller
{
    public function index() {
        return Inertia::render('SkinsIndex', [
            'skins' => Skin::all(),
            'groups' => SkinGroup::all(),
            'title' => 'Manage Skins',
        ]);
    }
    
    public function skinCreate(Request $request) {
        $validated = $request->validate([
            'name' => 'required|string|unique:skins'
        ]);
        
        $skin = Skin::create($validated);
        
        return redirect()->route('server.skins.skinGet', $skin);
    }
    
    public function skinGet(Skin $skin) {
        return Inertia::render('SkinsGet', [
            'skin' => $skin,
            'skinGroups' => $skin->groups,
            'availableGroups' => SkinGroup::all(),
            'title' => 'Edit Skin',
        ]);
    }
    
    public function skinUpdate(Request $request, Skin $skin) {
        $validated = $request->validate([
            'name' => [
                'required',
                'string',
                Rule::unique('skins')->ignore($skin),
            ],
            'head_id' => 'required|integer',
            'body_id' => 'required|integer',
            'foot_id' => 'required|integer',
            'hand_id' => 'required|integer',
            'usable_by_all' => 'boolean',
            'usable_by_admins' => 'boolean',
        ]);
        
        $validated_groups = $request->validate([
            'groups' => 'array',
            'groups.*.id' => 'required|integer',
        ]);
        
        $groups = [];
        foreach ($validated_groups['groups'] as $group) {
            if (!in_array($group['id'], $groups)) {
                $groups[] = $group['id'];
            }
        }
        
        $skin->groups()->sync($groups);
        
        $skin->fill($validated);
        $skin->save();
        
        return back();
    }
    
    public function skinDelete(Skin $skin) {
        $skin->delete();
        
        return redirect()->route('server.skins.index');
    }
    
    public function groupCreate(Request $request) {
        $validated = $request->validate([
            'name' => 'required|string|unique:skin_groups'
        ]);
        
        $group = SkinGroup::create($validated);
        
        return redirect()->route('server.skins.groupGet', $group);
    }
    
    public function groupGet(SkinGroup $group) {
        return Inertia::render('SkinsGroupGet', [
            'group' => $group,
            'players' => $group->players,
            'title' => 'Edit Skin Group',
        ]);
    }
    
    public function groupUpdate(Request $request, SkinGroup $group) {
        $validated = $request->validate([
            'oldPlayers' => 'array',
            'oldPlayers.*' => 'required|integer|exists:players,id',
            'newPlayers' => 'array',
            'newPlayers.*' => 'required|integer|exists:players,id',
        ]);
        
        $group->players()->sync(array_merge($validated['oldPlayers'], $validated['newPlayers']));
        
        return back();
    }
    
    public function groupDelete(SkinGroup $group) {
        $group->delete();
        
        return redirect()->route('server.skins.index');
    }
}

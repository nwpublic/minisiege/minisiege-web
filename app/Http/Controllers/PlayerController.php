<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use App\Models\Player;
use App\Jobs\CompileModule;

class PlayerController extends Controller
{
    public function leaderboard(Request $request) {
        $field_map = [
            'rank' => '#',
            'id' => 'GUID',
            'name' => 'Name',
            'kills' => 'Kills',
            'deaths' => 'Deaths',
            'teamkills' => 'Team Kills',
            'ratio' => 'K/D Ratio',
            'updated_at' => 'Last Active',
        ];
        
        $validated = $request->validate([
            'search' => 'nullable|string',
            'sort' => [
                'nullable',
                'string',
                Rule::in(array_keys($field_map)),
            ],
            'dir' => [
                'nullable',
                'string',
                Rule::in(['asc', 'desc']),
            ],
        ]);
        
        $search = $validated['search'] ?? null;
        $sort = $validated['sort'] ?? 'rank';
        $dir = $validated['dir'] ?? 'asc';
        
        $query = Player::select(array_keys($field_map))->where('rank', '!=', null)->orderBy($sort, $dir);
        
        if ($search !== null && $search !== '') {
            $query->where('name', 'like', "%$search%");
            $query->orWhere('id', '=', $search);
        }
        
        $players = $query->paginate(30)->withQueryString();
        
        return Inertia::render('Leaderboard', [
            'title' => 'Leaderboard',
            'players' => $players,
            'fields' => $field_map,
            'search' => $search,
            'sort' => $sort,
            'dir' => $dir,
        ]);
    }
    
    public function clearLeaderboard() {
        Player::clearScores();
        
        return redirect()->route('leaderboard');
    }
    
    public function get(Player $player) {
        return Inertia::render('PlayersView', [
            'title' => 'Player ' . $player->name,
            'player' => $player,
        ]);
    }
    
    public function adminsGet() {
        return Inertia::render('PlayersAdmins', [
            'players' => Player::where('admin', '>', '0')
                               ->orderBy('admin', 'desc')
                               ->orderBy('name', 'asc')
                               ->get(),
            'title' => 'Manage Admins',
        ]);
    }
    
    public function adminsUpdate(Request $request) {
        $validated = $request->validate([
            'removePlayers' => 'array',
            'removePlayers.*' => 'required|integer|exists:players,id',
            'newPlayers' => 'array',
            'newPlayers.*' => 'required|integer|exists:players,id',
            'superAdmins' => 'array',
            'superAdmins.*' => 'required|integer|exists:players,id',
        ]);
        
        Player::whereIn('id', $validated['newPlayers'])->update(['admin' => 1]);
        
        Player::where('admin', '>', 1)->update(['admin' => 1]);
        Player::whereIn('id', $validated['superAdmins'])->update(['admin' => 2]);
        
        Player::whereIn('id', $validated['removePlayers'])->update(['admin' => 0]);
        
        CompileModule::dispatch();
        
        return back();
    }
    
    public function mutesGet() {
        return Inertia::render('PlayersMutes', [
            'players' => Player::where('muted', '>', '0')
                               ->orderBy('name', 'asc')
                               ->get(),
            'title' => 'Manage Mutes',
        ]);
    }
    
    public function mutesUpdate(Request $request) {
        $validated = $request->validate([
            'removePlayers' => 'array',
            'removePlayers.*' => 'required|integer|exists:players,id',
            'newPlayers' => 'array',
            'newPlayers.*' => 'required|integer|exists:players,id',
        ]);
        
        Player::whereIn('id', $validated['removePlayers'])->update(['muted' => 0]);
        Player::whereIn('id', $validated['newPlayers'])->update(['muted' => 1]);
        
        CompileModule::dispatch();
        
        return back();
    }
}
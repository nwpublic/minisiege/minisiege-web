<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;

use App\Models\Setting;
use App\Jobs\CompileModule;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::before(function (JobProcessing $event) {
            if ($event->job->resolveName() === CompileModule::class) {
                Setting::set('compiling', true);
            }
        });

        Queue::after(function (JobProcessed $event) {
            if ($event->job->resolveName() === CompileModule::class) {
                Setting::set('compiling', false);
            }
        });
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Setting;

class FetchServerData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nwp:fetchstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the server status.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    private function get_server_status($address) {
        $curl = curl_init("http://{$address}/");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_HTTP09_ALLOWED, true);
        $response = curl_exec($curl);
        curl_close($curl);
        
        $status = [];
        
        if ($response) {
            // fix parsing
            $response = str_replace('&', '&amp;', $response);
            
            try {
                $xml = new \SimpleXMLElement($response);
                
                $status[] = ['Server Name', (string)$xml->Name];
                $status[] = ['Module', (string)$xml->ModuleName];
                $status[] = ['Current Map', (string)$xml->MapName];
                $status[] = ['Gamemode', (string)$xml->MapTypeName];
                $status[] = ['Players', "{$xml->NumberOfActivePlayers}/{$xml->MaxNumberOfPlayers}"];
            } catch (\Exception $e) {
                $status[] = ['Error', 'Error parsing server data'];
            }
        } else {
            $status[] = ['Error', 'Server did not respond'];
        }
        
        return $status;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $ip = Setting::get('serverAddress') ?? '127.0.0.1';
        $port = Setting::get('serverPort') ?? '7240';
        
        $status = $this->get_server_status("$ip:$port");
        Setting::set('status', $status);
        
        foreach ($status as $row) {
            echo "{$row[0]}: {$row[1]}\n";
        }
        
        return 0;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Models\Player;
use App\Models\Setting;
use App\Models\SkinGroup;
use App\Models\Skin;

class MigrateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nwp:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrate old panel's database.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $old_db = DB::connection('mysql-migrate');
        $chunk_size = 800;
        
        
        $this->comment('Migrating player_logs');
        $old_db->table('player_logs')->orderBy('id', 'asc')->chunk($chunk_size, function($players) {
            $inserts = [];
            foreach ($players as $player) {
                if (!$player->last_name) continue;
                
                $inserts[] = [
                    'id' => $player->id,
                    'name' => $player->last_name,
                    'kills' => $player->kills,
                    'deaths' => $player->deaths,
                    'teamkills' => $player->teamkills,
                    'slays' => $player->slays,
                    'kicks' => $player->kicks,
                    'temp_bans' => $player->temp_bans,
                    'perm_bans' => $player->perm_bans,
                    'face' => $player->face_keys,
                    'ip' => $player->last_ip,
                    'admin' => $player->admin,
                    'muted' => $player->muted,
                    'created_at' => $player->created_at,
                    'updated_at' => $player->updated_at,
                    
                    'ratio' => $player->deaths === 0 ? $player->kills : $player->kills/$player->deaths,
                ];
            }
            
            DB::table('players')->insert($inserts);
        });
        
        $this->comment('Calculating player ranks');
        Player::calculateRanks();
        
        $this->comment('Migrating server_maps');
        $old_db->table('server_maps')->orderBy('id', 'asc')->chunk($chunk_size, function($maps) {
            $inserts = [];
            foreach ($maps as $map) {
                $settings = json_decode($map->settings, 1);
                
                $image = null;
                if ($map->filename !== null) {
                    $file = new File(storage_path("app/images-migrate/$map->filename"));
                    Storage::putFile('public/images', $file);
                    $image = $file->hashName();
                }
                
                $skip_settings = [
                    'map_alt_name',
                    'set_map_min_players',
                    'set_map_max_players',
                    'set_map_admin_votable',
                    'set_map_player_votable',
                ];
                $new_settings = [];
                foreach ($settings as $key => $value) {
                    if (!in_array($key, $skip_settings)) {
                        if ($key === 'mission_type') {
                            $new_settings[$key] = str_replace('multiplayer_', '', $value);
                        } else {
                            $new_settings[$key] = $value;
                        }
                    }
                }
                
                $inserts[] = [
                    'id' => $map->id,
                    'string_id' => $map->game_id,
                    'name' => $map->name,
                    'custom_name' => $settings['map_alt_name'] ?? null,
                    'min_players' => $settings['set_map_min_players'] ?? null,
                    'max_players' => $settings['set_map_max_players'] ?? null,
                    'admin_votable' => $settings['set_map_admin_votable'] ?? false,
                    'player_votable' => $settings['set_map_player_votable'] ?? false,
                    'settings' => json_encode($new_settings),
                    'image' => $image,
                    'created_at' => $map->created_at,
                    'updated_at' => $map->updated_at,
                ];
            }
            
            DB::table('maps')->insert($inserts);
        });
        
        $this->comment('Migrating server_settings');
        $old_settings = $old_db->table('server_settings')->get();
        $new_settings = [];
        foreach ($old_settings as $setting) {
            if ($setting->setting === 'welcome') {
                $new_settings['serverWelcome'] = $setting->value;
            } elseif ($setting->setting === 'messages') {
                $new_settings['serverAnnouncements'] = explode("\n", $setting->value);
            }
        }
        Setting::applySettings($new_settings);
        
        $this->comment('Migrating server_groups');
        $old_groups = $old_db->table('server_groups')->get();
        foreach ($old_groups as $group) {
            $new_group = new SkinGroup;
            $new_group->id = $group->id;
            $new_group->name = $group->name;
            $new_group->created_at = $group->created_at;
            $new_group->updated_at = $group->updated_at;
            $new_group->save();
            
            $new_group->players()->sync(json_decode($group->members, 1));
        }
        
        $this->comment('Migrating server_skins');
        $old_skins = $old_db->table('server_skins')->get();
        foreach ($old_skins as $skin) {
            $old_groups = json_decode($skin->permissions, 1)['groups'];
            $new_groups = [];
            $usable_by_all = false;
            $usable_by_admins = false;
            foreach($old_groups as $group) {
                if ($group > 0) {
                    $new_groups[] = $group;
                } else {
                    if ($group == -1) {
                        $usable_by_admins = true;
                    } elseif ($group == -2) {
                        $usable_by_all = true;
                    }
                }
            }
            
            $new_skin = new Skin;
            $new_skin->name = $skin->name;
            $new_skin->head_id = $skin->head_id;
            $new_skin->body_id = $skin->body_id;
            $new_skin->foot_id = $skin->foot_id;
            $new_skin->hand_id = $skin->hand_id;
            $new_skin->usable_by_all = $usable_by_all;
            $new_skin->usable_by_admins = $usable_by_admins;
            $new_skin->created_at = $skin->created_at;
            $new_skin->updated_at = $skin->updated_at;
            $new_skin->save();
            
            $new_skin->groups()->sync($new_groups);
        }
        
        return 0;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use App\Models\Map;
use App\Models\Player;
use App\Models\Setting;

class CompileModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nwp:compile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile game files.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    protected $source_path = 'nw-source';
    
    protected function isAssoc($array) {
        if ($array === []) return false;
        return array_keys($array) !== range(0, count($array) - 1);
    }
    
    protected function dataToString($data, $force_assoc=false) {
        $string = '';
        
        if (is_array($data)) {
            if ($force_assoc || $this->isAssoc($data)) {
                $string .= '{';
                
                foreach ($data as $key => $value) {
                    $string .= $this->dataToString($key) . ':' . $this->dataToString($value) . ',';
                }
                
                $string .= '}';
            } else {
                $string .= '[';
                
                foreach ($data as $value) {
                    $string .= $this->dataToString($value) . ',';
                }
                
                $string .= ']';
            }
        } elseif (is_integer($data)) {
            $string .= $data;
        } else { // string
            $data = str_replace("\\", "\\\\", $data);
            $data = str_replace("'", "\\'", $data);
            $string .= "'$data'";
        }
        
        return $string;
    }
    
    protected function writeToFile($filename, $variable, $data, $append=false, $force_assoc=false) {
        $string = $variable . '=' . $this->dataToString($data, $force_assoc);
        
        $file = "$this->source_path/$filename";
        
        if ($append) {
            Storage::append($file, $string);
        } else {
            $string = "# coding=utf-8\n" . $string;
            
            Storage::put($file, $string);
        }
    }
    
    protected function writeConfigs() {
        $maps = Map::all();
        
        $dict = [];
        foreach ($maps as $map) {
            $settings = [];
            
            if ($map->custom_name !== null) {
                $settings['map_alt_name'] = $map->custom_name;
            }
            
            if ($map->min_players !== null) {
                $settings['set_map_min_players'] = $map->min_players;
            }
            
            if ($map->max_players !== null) {
                $settings['set_map_max_players'] = $map->max_players;
            }
            
            if ($map->admin_votable) {
                $settings['set_map_admin_votable'] = $map->admin_votable;
            }
            
            if ($map->player_votable) {
                $settings['set_map_player_votable'] = $map->player_votable;
            }
            
            if (count($map->settings) > 0) {
                foreach ($map->settings as $key => $value) {
                    if ($value === null) continue;
                    
                    if ($key === 'mission_type') {
                        $settings[$key] = 'multiplayer_' . $value;
                    } else {
                        $settings[$key] = (integer)$value;
                    }
                }
            }
            
            if (count($settings) > 0) {
                $dict[$map->id] = $settings;
            }
        }
        $this->writeToFile('config_maps.py', 'config_map_settings', $dict, false, true);
        
        $admins = Player::where('admin', '>', 0)->get();
        $array = [];
        foreach ($admins as $admin) {
            $array[] = ($admin->id | (($admin->admin - 1) << 4*6));
        }
        $this->writeToFile('config_admins.py', 'config_admins', $array);
        
        $muted_players = Player::where('muted', true)->get();
        $array = [];
        foreach ($muted_players as $player) {
            $array[] = $player->id;
        }
        $this->writeToFile('config_punishments.py', 'config_muted', $array);
        
        $dict = [];
        $dict['map_rotation'] = [];
        
        $welcome_message = Setting::get('serverWelcome');
        if ($welcome_message === null) {
            $welcome_message = ' ';
        }
        $welcome_message = str_replace("\n", '^', $welcome_message);
        $welcome_message = str_replace("\r", '', $welcome_message);
        $dict['welcome'] = $welcome_message;
        
        $announcements = Setting::get('serverAnnouncements');
        if ($announcements === null) {
            $announcements = [];
        }
        $dict['messages'] = implode('^', $announcements);
        $this->writeToFile('config_server.py', 'config_server_settings', $dict, false, true);
        
        $server_domain = explode('//', env('APP_URL'), 2)[1];
        $this->writeToFile('const_staging.py', 'server_domain', $server_domain);
        $this->writeToFile('const_staging.py', 'server_api_key', Setting::get('serverApiKey') ?? '', true);
        
        $shell_config = 'config_web.sh';
        $this->writeToFile($shell_config, 'type', env('COMPILE_UPLOAD_TYPE'));
        $this->writeToFile($shell_config, 'address', env('COMPILE_UPLOAD_ADDRESS'), true);
        $this->writeToFile($shell_config, 'port', env('COMPILE_UPLOAD_PORT'), true);
        $this->writeToFile($shell_config, 'username', env('COMPILE_UPLOAD_USER'), true);
        $this->writeToFile($shell_config, 'password', env('COMPILE_UPLOAD_PASS'), true);
        $this->writeToFile($shell_config, 'path', env('COMPILE_UPLOAD_PATH'), true);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->writeConfigs();
        
        chdir(storage_path("app/$this->source_path"));
        
        $process = new Process(['./compile_module_web.sh']);
        $process->run(function ($type, $buffer) {
            echo $buffer;
        });
        
        return 0;
    }
}

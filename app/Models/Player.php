<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Player extends Model
{
    use HasFactory;
    
    protected $fillable = ['id', 'name', 'ip', 'face'];
    
    protected $attributes = [
        'kills' => 0,
        'deaths' => 0,
        'teamkills' => 0,
        'admin' => 0,
        'muted' => 0,
        'slays' => 0,
        'kicks' => 0,
        'temp_bans' => 0,
        'perm_bans' => 0,
    ];
    
    public function skinGroups() {
        return $this->belongsToMany(SkinGroup::class);
    }
    
    public function skins() {
        if ($this->admin > 1) {
            $skins = Skin::all();
        } else {
            $query = Skin::where('usable_by_all', 1);
            if ($this->admin > 0) {
                $query->orWhere('usable_by_admins', 1);
            }
            $skins = $query->get();
            
            foreach ($this->skinGroups as $group) {
                $skins = $skins->merge($group->skins);
            }
        }
        
        return $skins;
    }
    
    protected static function booted() {
        static::saving(function (Player $player) {
            $player->calculateRatio();
        });
    }
    
    public static function clearScores() {
        DB::table('players')->update([
            'kills' => 0,
            'deaths' => 0,
            'teamkills' => 0,
            'ratio' => 0,
        ]);
    }
    
    public static function calculateRanks() {
        DB::statement(DB::raw('set @rank=0'));
        
        DB::table('players')
            ->orderBy('kills', 'desc')
            ->orderBy('deaths', 'asc')
            ->orderBy('teamkills', 'asc')
            ->orderBy('updated_at', 'desc')
            ->update([
                    'rank' => DB::raw('@rank := @rank + 1')
                ]);
    }
    
    public function calculateRatio() {
        if ($this->deaths === null) {
            $this->ratio = 0;
        } elseif ($this->deaths === 0) {
            $this->ratio = $this->kills;
        } else {
            $this->ratio = round($this->kills / $this->deaths, 2);
        }
    }
}

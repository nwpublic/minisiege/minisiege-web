<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'setting';
    public $incrementing = false;
    protected $fillable = ['setting', 'value'];
    protected $casts = [
        'value' => 'json',
    ];
    
    public static function applySettings($settings) {
        $upserts = [];
        
        foreach ($settings as $setting => $value) {
            $upserts[] = [
                'setting' => $setting,
                'value' => json_encode($value),
            ];
        }
        
        static::upsert($upserts, ['setting'], ['value']);
    }
    
    public static function set($key, $value) {
        static::updateOrCreate(
            ['setting' => $key],
            ['value' => $value]
        );
    }
    
    public static function get($key) {
        $setting = static::find($key);
        if ($setting === null) {
            return null;
        } else {
            return $setting->value;
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'custom_name',
        'min_players',
        'max_players',
        'admin_votable',
        'player_votable',
        'settings',
    ];
    
    protected $casts = [
        'settings' => 'json',
    ];
}

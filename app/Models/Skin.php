<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skin extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'head_id',
        'body_id',
        'foot_id',
        'hand_id',
        'usable_by_all',
        'usable_by_admins',
    ];
    
    public function groups() {
        return $this->belongsToMany(SkinGroup::class);
    }
}

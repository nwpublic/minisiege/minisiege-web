<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkinGroup extends Model
{
    use HasFactory;
    
    protected $fillable = ['name'];
    
    public function skins() {
        return $this->belongsToMany(Skin::class);
    }
    
    public function players() {
        return $this->belongsToMany(Player::class);
    }
}
